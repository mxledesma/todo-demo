package dev.rehm;

import java.time.LocalDate;

public class Task {

    String taskName;
    boolean isCompleted;
    String description;
    LocalDate date;

    public Task(){
        super();
    }

    public Task(String taskName){
        super();
        this.taskName=taskName;
    }

    public Task(String taskName, boolean isCompleted){
        super();
        this.taskName = taskName;
        this.isCompleted = isCompleted;
    }
}